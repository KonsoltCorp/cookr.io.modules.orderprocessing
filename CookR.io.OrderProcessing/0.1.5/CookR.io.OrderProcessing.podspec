#
# Be sure to run `pod lib lint CookR.io.OrderProcessing.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CookR.io.OrderProcessing'
  s.version          = '0.1.5'
  s.summary          = 'The OrderProcessing module for CookR.io'

  s.description      = 'The order processing module is an example framework for CookR.io that handles order capturing and processing'

  s.homepage         = 'git@bitbucket.org:KonsoltCorp/cookr.io.modules.orderprocessing.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Martello Jones' => 'martello@konsoltcorp.com' }
  s.source           = { :git => 'git@bitbucket.org:KonsoltCorp/cookr.io.modules.orderprocessing.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/KonsoltCorp'

  s.ios.deployment_target = '9.0'
  s.source_files = 'CookR.io.OrderProcessing/Classes/**/*'
  
  s.resource_bundles = {
        'CookR.io.OrderProcessing' => [
        'CookR.io.OrderProcessing/Assets/**/*',
        'CookR.io.OrderProcessing/Resources/**/*'
    ]
  }

end
