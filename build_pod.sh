#!/bin/sh

REPO_NAME=CookR.io.OrderProcessing
GIT_REPO=https://bitbucket.org/KonsoltCorp/cookr.io.modules.orderprocessing

#Ensure the spec repo is locally added
pod repo add ${REPO_NAME} ${GIT_REPO} 

pod repo push ${REPO_NAME} ${REPO_NAME}.podspec --verbose --allow-warnings
